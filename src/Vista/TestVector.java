/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.VectorEnteros;

/**
 *
 * @author madar
 */
public class TestVector {
    public static void main(String[] args) {
        Consola teclado=new Consola();
        String cadena=teclado.leerCadena("Digite una cadena del tipo: n&limite (ejemplo: 5&4)");
        VectorEnteros v1=new VectorEnteros(cadena);
        System.out.println("El vector creado fue:"+v1.toString());
        v1.alterar();
        System.out.println("Vector alterado:"+v1.toString());
        
    }
}
